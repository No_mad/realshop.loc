-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 29 2021 г., 14:27
-- Версия сервера: 5.7.36-0ubuntu0.18.04.1
-- Версия PHP: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id_categories` int(11) NOT NULL,
  `name_categories` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id_categories`, `name_categories`, `createdAt`, `updatedAt`) VALUES
(1, 'notFound', '2021-10-28 10:16:05', '2021-10-28 10:16:05'),
(2, 'Stationery', '2021-10-28 11:20:59', '2021-10-28 11:20:59'),
(3, 'Notebooks', '2021-10-28 12:24:05', '2021-10-28 12:24:05');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id_orders` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `id_products` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id_orders`, `id_users`, `id_products`, `createdAt`, `updatedAt`) VALUES
(1, 3, 2, '2021-10-29 14:24:50', '2021-10-29 14:24:50'),
(2, 3, 5, '2021-10-29 14:25:48', '2021-10-29 14:25:48');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id_products` int(11) NOT NULL,
  `name_products` varchar(255) NOT NULL,
  `cost_products` decimal(10,2) NOT NULL,
  `image_products` varchar(255) NOT NULL DEFAULT 'https://oda.ztmbk.gov.ua/upload/subject/fields/50/2020-12-24/7884_1608805894.png',
  `id_categories` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id_products`, `name_products`, `cost_products`, `image_products`, `id_categories`, `createdAt`, `updatedAt`) VALUES
(2, 'Nicer pencil', '0.11', 'https://pngimg.com/uploads/pencil/pencil_PNG3861.png', 2, '2021-10-28 11:26:14', '2021-10-28 14:20:09'),
(3, 'Pen Schneider K15', '0.45', 'https://5print.ua/content/images/43/1800x1800l80nn0/ruchka-026-44903865635028.webp', 2, '2021-10-28 12:16:07', '2021-10-28 12:16:07'),
(4, 'HP Pavilion Laptop 15-eg0037ua Ceramic White (444M5EA)', '716.60', 'https://i.citrus.ua/imgcache/size_800/uploads/shop/9/9/9909b05f1cc5a9c44d60c9648fae2e41.jpg', 3, '2021-10-28 12:25:10', '2021-10-28 12:25:10'),
(5, 'Asus ZenBook Pro 15 UX535LH-BN121T (90NB0RX2-M02890) Pine Grey', '1320.00', 'https://content.rozetka.com.ua/goods/images/original/196674985.jpg', 3, '2021-10-28 12:29:36', '2021-10-28 12:29:36');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `name_users` varchar(255) NOT NULL,
  `email_users` varchar(255) NOT NULL,
  `password_users` varchar(255) NOT NULL,
  `role_users` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_users`, `name_users`, `email_users`, `password_users`, `role_users`, `createdAt`, `updatedAt`) VALUES
(3, 'jhon', 'jhon@test.com', '$2y$10$T2Yc0i5Bo.F.2CLRbNHO5uX3xIQ2N3d9NVGi1w6wa6JV4kXlh92nq', 'user', '2021-10-27 18:41:17', '2021-10-27 18:41:17'),
(4, 'admin', 'admin@test.com', '$2y$10$LjDZIcNj1a3Inu7VXXCJdOQS6E5m4RGbFJVamdkZqWyxBZ9c7GqgG', 'admin', '2021-10-28 08:49:37', '2021-10-28 08:49:57');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_categories`),
  ADD UNIQUE KEY `name_categories` (`name_categories`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_orders`),
  ADD KEY `id_users` (`id_users`),
  ADD KEY `id_products` (`id_products`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_products`),
  ADD KEY `id_categories` (`id_categories`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD UNIQUE KEY `email_users` (`email_users`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id_categories` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id_orders` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id_products` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`id_products`) REFERENCES `products` (`id_products`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id_categories`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

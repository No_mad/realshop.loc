<?php

namespace RealShop\Controllers;

use RealShop\Views\View;

class BaseController
{
    private View $view;

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * @return View
     */
    public function getView(): View
    {
        return $this->view;
    }
}
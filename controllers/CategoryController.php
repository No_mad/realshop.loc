<?php

namespace RealShop\Controllers;

use RealShop\Models\Categories;

class CategoryController extends BaseController
{
    public function addCategory(array $request = null)
    {
        $error = '';
        if (!empty($request)) {
            $newCategory = new Categories($request['name']);
            $result = $newCategory->addCategory();
            if ($result['success']) {
                header('Location: /');
            } else {
                $error = $result['error'];
            }
        }
        $this->getView()->render('addCategory', [], ['error' => $error]);
    }
}
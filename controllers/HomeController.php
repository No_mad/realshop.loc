<?php

namespace RealShop\Controllers;

class HomeController extends BaseController
{
    public function about()
    {
        $this->getView()->render('about', [], []);
    }
}
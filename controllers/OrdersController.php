<?php

namespace RealShop\Controllers;

use RealShop\Models\Order;
use RealShop\Models\Product;
use RealShop\Models\Reorder;

class OrdersController extends BaseController
{
    public function successPayment(array $request = null)
    {
        if (!empty($request['item_number'])) {
            $newReorder = new Reorder($_SESSION['user']->getId(), $request['item_number']);
            $newReorder->successPayment();
        } elseif (!empty($_SESSION['shoppingCart'])) {
            foreach ($_SESSION['shoppingCart'] as $order) {
                $newOrder = new Order($_SESSION['user']->getId(), $order['id_products']);
                $newOrder->successPayment();
            }
        }
        header("Location: /");
    }

    public function cancelPayment()
    {
        $_SESSION['shoppingCart'] = [];
        header("Location: /");
    }

    public function getUserOrders()
    {
        $error = '';
        $data = [];
        if (!empty($_SESSION['user'])) {
            $result = Order::getUserOrders($_SESSION['user']->getId());
            if ($result['success']) {
                $data = $result['data'];
            } else {
                $error = $result['error'];
            }
        }
        $this->getView()->render('orders', $data, ['error' => $error]);
    }

    public function getUserOrdersInFile(){
        if (!empty($_SESSION['user'])) {
            $result = Order::getUserOrders($_SESSION['user']->getId());
            if ($result['success']) {
                $content = "";
                foreach ($result['data'] as $row) {
                    $product = Product::getProductById($row['id_products']);
                    if (!empty($product)) {
                        $content .= $row['id_orders'].','.$product['name_products'].','.$product['cost_products']."\n";
                    }
                }
                file_put_contents($_ENV['FILE_NAME'],$content); //it's possible without creating file

                if (file_exists($_ENV['FILE_NAME'])) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="'.basename($_ENV['FILE_NAME']).'"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($_ENV['FILE_NAME']));
                    readfile($_ENV['FILE_NAME']);
                }
            }
        }
    }
}
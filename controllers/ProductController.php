<?php

namespace RealShop\Controllers;

use RealShop\Models\Categories;
use RealShop\Models\Product;

class ProductController extends BaseController
{
    public function addProduct(array $request = null)
    {
        if ($_SESSION['user']->getRole() === 'admin') {
            $error = '';
            if (!empty($request)) {
                if (Categories::isCategoryExists($request['category'])) {
                    $newProduct = new Product(
                        $request['name'],
                        $request['cost'],
                        $request['image'],
                        $request['category']
                    );
                    $result = $newProduct->addProduct();
                    if ($result['success']) {
                        header('Location: /');
                    } else {
                        $error = $result['error'];
                    }
                } else {
                    $error = 'Category with entered name doesn\'t exist';
                }
            }
            $this->getView()->render('addProduct', [], ['error' => $error]);
        } else {
            http_response_code(404);
        }
    }

    public function getAllProducts(array $request = null)
    {
        $data = [];
        $error = '';
        if (!empty($request)) {
            $result = Product::getProductsByName($request['search']);
        } else {
            $result = Product::getAllProducts();
        }
        if ($result['success']) {
            $data = $result['data'];
        } else {
            $error = $result['error'];
        }
        $this->getView()->render('home', ['products' => $data], ['error' => $error]);
    }


    public function updateProduct(array $request = null)
    {
        if ($_SESSION['user']->getRole() === 'admin') {
            $error = '';
            if (!empty($request['name'])) {
                $newProduct = new Product($request['name'], $request['cost'], $request['image'], $request['category']);
                $result = $newProduct->updateProduct($request['id']);
                if ($result['success']) {
                    header('Location: /');
                } else {
                    $error = $result['error'];
                }
            }
            $this->getView()->render('updateProduct', [], ['error' => $error]);
        } else {
            http_response_code(404);
        }
    }

    public function deleteProduct(array $request = null)
    {
        if ($_SESSION['user']->getRole() === 'admin') {
            if (!empty($request)) {
                $result = Product::deleteProduct($request['id']);
                header('Location: /');
            }
        } else {
            http_response_code(404);
        }
    }

    public function addToCart(array $request = null)
    {
        if (!empty($request)) {
            Product::addProductById($request['id']);
        }
    }
}
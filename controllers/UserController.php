<?php

namespace RealShop\Controllers;

use RealShop\Models\User;

class UserController extends BaseController
{
    public function login(array $request = null)
    {
        $error = '';
        if (empty($_SESSION['user'])) {
            if (!empty($request)) {
                $result = User::login($request['email'], $request['password']);
                if ($result['success']) {
                    $_SESSION['user'] = $result['data'];
                    $_SESSION['shoppingCart'] = [];
                    header('Location: /');
                } else {
                    $error = $result['error'];
                }
            }
        } else {
            header('Location: /');
        }
        $this->getView()->render('login', [], ['error' => $error]);
    }

    public function register(array $request = null)
    {
        if (empty($_SESSION['user'])) {
            $error = '';
            if (!empty($request)) {
                if (strlen($request['password']) >= 4) {
                    $newUser = new User($request['username'], $request['email'], $request['password'], 'user');
                    $result = $newUser->register();
                    if ($result['success']) {
                        header('Location: /login');
                    } else {
                        $error = $result['error'];
                    }
                } else {
                    $error = 'Password too small (4 symbols needed)';
                }
            }
            $this->getView()->render('register', [], ['error' => $error]);
        }else{
            header('Location: /');
        }
    }

    public function logout(){
        unset($_SESSION['user']);
        unset($_SESSION['shoppingCart']);
        header('Location: /login');
    }
}
<?php

namespace RealShop\Models;

class Categories
{
    private int $id;
    private string $name;

    public function __construct(string $name, int $id = null)
    {
        if (!is_null($id)) {
            $this->id = $id;
        }
        $this->name = $name;
    }

    public static function getCategoryByName(string $name): ?Categories
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `categories` WHERE `name_categories` = :name'
        );
        if ($stmt->execute(['name' => $name])) {
            $data = $stmt->fetchAll();
            $result = $data[0];
            return new Categories($result['name_categories'], $result['id_categories']);
        }
        return null;
    }

    public static function isCategoryExists(string $name): bool
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `categories` WHERE `name_categories` = :name'
        );
        if ($stmt->execute(['name' => $name])) {
            return true;
        }
        return false;
    }

    public function addCategory()
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'INSERT INTO `categories` (`name_categories`) VALUES (:name)'
        );
        if ($stmt->execute(['name' => $this->name])) {
            return ['success' => true];
        }
        return ['success' => false, 'error' => 'Something went wrong when creating new category'];
    }

    public static function getAllCategories()
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `categories`'
        );
        $stmt->execute();
        if ($data = $stmt->fetchAll()) {
            return $data;
        }
        return [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
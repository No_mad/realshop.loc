<?php

namespace RealShop\Models;

use PDO;

class DB
{
    private static $instance;

    public function __construct()
    {
        self::$instance = new PDO(
            "mysql:host=localhost;port=3306;dbname=" . $_ENV['DB_NAME'], $_ENV['DB_USER'], $_ENV['DB_PASS'], [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]
        );
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            new self();
        }
        return self::$instance;
    }
}
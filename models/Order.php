<?php

namespace RealShop\Models;

class Order
{
    private int $id;
    private int $idUser;
    private int $idProduct;

    public function __construct(int $idUser, int $idProduct, int $id = null)
    {
        if (!is_null($id)) {
            $this->id = $id;
        }
        $this->idUser = $idUser;
        $this->idProduct = $idProduct;
    }

    public function successPayment()
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'INSERT INTO `orders`(`id_users`,`id_products`) VALUES (:idUsers, :idProducts)'
        );
        if ($stmt->execute(['idUsers' => $this->idUser, 'idProducts' => $this->idProduct])) {
            return true;
        }
        return false;
    }

    public static function getUserOrders(int $idUser)
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `orders` WHERE `id_users` = :id'
        );
        if ($stmt->execute(['id' => $idUser])) {
            $data = $stmt->fetchAll();
            return ['success' => true, 'data' => $data];
        }
        return ['success' => false, 'error' => 'Something went horribly wrong when fetching all user orders'];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser(int $idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return int
     */
    public function getIdProduct(): int
    {
        return $this->idProduct;
    }

    /**
     * @param int $idProduct
     */
    public function setIdProduct(int $idProduct): void
    {
        $this->idProduct = $idProduct;
    }
}
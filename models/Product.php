<?php

namespace RealShop\Models;

use RealShop\Models\Categories;

class Product
{
    private int $id;
    private string $name;
    private float $cost;
    private string $image;
    private Categories $category;

    public function __construct(string $name, float $cost, string $image, string $nameCategory, int $id = null)
    {
        if (!is_null($id)) {
            $this->id = $id;
        }
        $this->name = $name;
        $this->cost = $cost;
        $this->image = $image;
        $this->setCategory($nameCategory);
    }

    public function addProduct(): array
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'INSERT INTO `products` (`name_products`, `cost_products`, `image_products`, `id_categories`)
                                    VALUES (:name, :cost, :image, :idCategories)'
        );
        if ($stmt->execute([
            'name' => $this->name,
            'cost' => $this->cost,
            'image' => $this->image,
            'idCategories' => $this->category->getId()
        ])) {
            return ['success' => true];
        }
        return ['success' => false, 'error' => 'Something went wrong when creating new product'];
    }

    public static function getAllProducts(): array
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `products`'
        );
        if ($stmt->execute()) {
            if (!empty($data = $stmt->fetchAll())) {
                return ['success' => true, 'data' => $data];
            } else {
                return ['success' => false, 'error' => 'No products found'];
            }
        }
        return ['success' => false, 'error' => 'Something went wrong when fetching products'];
    }

    public static function getProductsByName(string $name): array
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `products` WHERE `name_products` LIKE :name'
        );
        if ($stmt->execute(['name' => '%' . $name . '%'])) {
            if (!empty($data = $stmt->fetchAll())) {
                return ['success' => true, 'data' => $data];
            } else {
                return ['success' => false, 'error' => 'No products found'];
            }
        }
        return ['success' => false, 'error' => 'Something went wrong when fetching products'];
    }

    public function updateProduct(int $id): array
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'UPDATE `products` SET `name_products` = :name, `cost_products` = :cost, 
                                        `image_products` = :image, `id_categories` = :idCategory
                                     WHERE `id_products` = :id'
        );
        if ($stmt->execute([
            'name' => $this->name,
            'cost' => $this->cost,
            'image' => $this->image,
            'idCategory' => $this->category->getId(),
            'id' => $id
        ])) {
            return ['success' => true];
        }
        return ['success' => false, 'error' => 'Something went wrong when updating product'];
    }

    public static function deleteProduct(int $id): array
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'DELETE FROM `products` WHERE `id_products` = :id'
        );
        if ($stmt->execute(['id' => $id])) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public static function addProductById(int $id)
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `products` WHERE `id_products` = :id'
        );
        if ($stmt->execute(['id' => $id])) {
            $data = $stmt->fetchAll()[0];
            array_push($_SESSION['shoppingCart'], $data);
        }
    }

    public static function getProductById(int $id)
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'SELECT * FROM `products` WHERE `id_products` = :id'
        );
        if($stmt->execute(['id' => $id])){
            $data = $stmt->fetchAll()[0];
            return $data;
        }
        return [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     */
    public function setCost(float $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return \RealShop\Models\Categories|null
     */
    public function getCategory(): ?\RealShop\Models\Categories
    {
        return $this->category;
    }

    /**
     * @param string $nameCategory
     */
    public function setCategory(string $nameCategory): void
    {
        if (!is_null($data = Categories::getCategoryByName($nameCategory))) {
            $this->category = $data;
        } else {
            $this->category = Categories::getCategoryByName('notFound');
        }
    }
}
<?php

namespace RealShop\Models;

class Reorder
{
    private int $id;
    private int $idUser;
    private int $idProduct;

    public function __construct(int $idUser, int $idProduct, int $id = null)
    {
        if (!is_null($id)) {
            $this->id = $id;
        }
        $this->idUser = $idUser;
        $this->idProduct = $idProduct;
    }

    public function successPayment()
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            'INSERT INTO `reorders`(`id_users`,`id_products`) VALUES (:idUsers, :idProducts)'
        );
        if ($stmt->execute(['idUsers' => $this->idUser, 'idProducts' => $this->idProduct])) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser(int $idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return int
     */
    public function getIdProduct(): int
    {
        return $this->idProduct;
    }

    /**
     * @param int $idProduct
     */
    public function setIdProduct(int $idProduct): void
    {
        $this->idProduct = $idProduct;
    }


}
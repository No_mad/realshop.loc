<?php

namespace RealShop\Models;

use RealShop\Models\DB;

class User
{
    private int $id;
    private string $username;
    private string $email;
    private string $password;
    private string $role;

    public function __construct(string $username, string $email, string $password, string $role, int $id = null)
    {
        if (!is_null($id)) {
            $this->id = $id;
        }
        $this->username = $username;
        $this->email = $email;
        $this->setPassword($password);
        $this->role = $role;
    }

    public static function login(string $email, string $password)
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            "SELECT * FROM `users` WHERE BINARY `email_users` = :email"
        );
        if ($stmt->execute(['email' => $email])) {
            $data = $stmt->fetchAll();
            $result = $data[0];
            if (!empty($result)) {
                if (password_verify($password, $result['password_users'])) {
                    return [
                        'data' => new User(
                            $result['name_users'], $result['email_users'], $result['password_users'],
                            $result['role_users'], $result['id_users']
                        ),
                        'success' => true
                    ];
                } else {
                    return ['success' => false, 'error' => 'bruh'];
                }
            } else {
                return ['success' => false, 'error' => 'Incorrect email and password combination'];
            }
        }
        return ['success' => false, 'error' => $pdo->errorInfo()[2]];
    }

    public function register()
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare(
            "INSERT INTO `users` (`name_users`,`email_users`,`password_users`,`role_users`) 
                                VALUES (:username, :email, :password, :role)"
        );
        if ($stmt->execute([
            'username' => $this->getUsername(),
            'email' => $this->getEmail(),
            'password' => $this->getPassword(),
            'role' => $this->getRole()
        ])) {
            return ['success' => true];
        }
        return ['success' => false, 'error' => 'Email must be unique'];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }


    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }
}
$(function (){
  $("form[id^=paypalForm]").submit(function (e){
      e.preventDefault();
      let id = $(this).parent().children("#idProduct").val();
      if(id){
        $.ajax({
          url: '/addToCart?id=' + id,
          method: 'GET',
          success: function (){
            e.currentTarget.submit();
          }
        })
      }
  });
});
<?php


use RealShop\Routers\BaseRouter;

define('ROOT_PATH', dirname(__FILE__, 2));
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

$dotenv = \Dotenv\Dotenv::createImmutable(ROOT_PATH);
$dotenv->load();
session_start();

$request = $_REQUEST ?? [];
if (!empty($_SESSION['user'])) {
    $path = $request['path'] ?? '/';
} else {
    $path = ($request['path'] == 'register') ? 'register' : 'login';
}
unset ($request['path']);

$router = new BaseRouter();
$router->process($path, $request);

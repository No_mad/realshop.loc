<?php

namespace RealShop\Routers;

class BaseRouter
{
    private array $routes = [];

    public function __construct()
    {
        $this->setRoute('/','ProductController','getAllProducts');
        $this->setRoute('login', 'UserController', 'login');
        $this->setRoute('register', 'UserController', 'register');
        $this->setRoute('logout', 'UserController', 'logout');
        $this->setRoute('about', 'HomeController', 'about');
        $this->setRoute('addProduct', 'ProductController', 'addProduct');
        $this->setRoute('addCategory', 'CategoryController', 'addCategory');
        $this->setRoute('updateProduct', 'ProductController', 'updateProduct');
        $this->setRoute('deleteProduct', 'ProductController', 'deleteProduct');
        $this->setRoute('addToCart', 'ProductController', 'addToCart');
        $this->setRoute('success', 'OrdersController', 'successPayment');
        $this->setRoute('cancel', 'OrdersController', 'cancelPayment');
        $this->setRoute('myOrders', 'OrdersController', 'getUserOrders');
        $this->setRoute('exportMyOrders', 'OrdersController', 'getUserOrdersInFile');
    }

    public function setRoute(string $url, string $controller, string $action): bool
    {
        if (empty($this->routes[$url])) {
            $this->routes[$url] = ['controller' => $controller, 'action' => $action];
            return true;
        }
        return false;
    }

    /**
     * @param string $url
     * @return array
     */
    public function getRoute(string $url): array
    {
        return $this->routes[$url] ?? [];
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function process(string $url, array $request)
    {
        $route = $this->getRoute($url);
        if(!empty($route)){
            $controllerName = "RealShop\\Controllers\\". $route['controller'];
            $controller = new $controllerName();
            $controller->{$route['action']}($request);
        }
    }
}
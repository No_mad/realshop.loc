<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <?php require_once 'include_css.php'?>
</head>
<body class="bg-secondary">
    <?php require_once 'header.php'?>
    <div class="container position-absolute top-50 start-50 translate-middle w-50">
        <div class="form-control border border-3 border-dark">
            <div class="d-flex justify-content-center">
                <h1>About us</h1>
            </div>
            <div class="row">
                <div class="col-4">
                    <img class="rounded float-start" src="img/image.jpg" width="200px" height="200px" alt="">
                </div>
                <div class="col-8">
                    <p>
                        This cat single handedly created this site. Very dangerous specimen.
                        Better be careful when trying to break or hack into this site.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php require_once 'footer.php'?>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add new category</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php require_once 'include_css.php' ?>
</head>
<body class="bg-secondary">
<?php require_once 'header.php' ?>
<div class="container-fluid">
    <div class="container position-absolute top-50 start-50 translate-middle w-50">
        <form action="/addCategory" method="post" class="form-control border border-3 border-dark">
            <div class="d-flex justify-content-center">
                <h1>Add category</h1>
            </div>
            <label for="nameCategory">Name:</label>
            <input type="text" id="nameCategory" name="name" class="form-control" placeholder="Enter name" required>
            <?php if(!empty($errors['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $errors['error']; ?>
                </div>
            <?php endif; ?>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-outline-warning text-black w-25 mt-sm-3">Add new category</button>
            </div>
        </form>
    </div>
</div>
<?php require_once 'footer.php' ?>
</body>
</html>



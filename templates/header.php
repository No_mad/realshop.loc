<header class="p-3 bg-dark text-white">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="d-flex justify-content-start">
                    <h3>Real Shop</h3>
                    <ul class="nav nav-tabs ms-sm-5 col-12 col-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="/">
                                Home
                            </a>
                        </li>
                        <?php if((!empty($_SESSION['user']))&&($_SESSION['user']->getRole() === 'admin')): ?>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="/addProduct">
                                Add product
                            </a>
                        </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="/addCategory">
                                    Add category
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-4">
                <?php if(!empty($_SESSION['user'])): ?>
                    <div class="d-flex justify-content-end justify-content-evenly">
                        <form action="/" method="get">
                            <input type="search" name="search" class="form-control bg-secondary bg-opacity-25 text-white" placeholder="Search" aria-label="Search" required>
                        </form>

                        <div class="dropdown">
                            <button class="btn btn-outline-warning text-white dropdown-toggle" type="button"
                                    id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <?php echo $_SESSION['user']->getUsername();?>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="/myOrders">My orders</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="/logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>

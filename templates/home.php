<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <?php require_once 'include_css.php'?>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="bg-secondary">
<?php require_once 'header.php'?>
    <div  class="container pb-5">
        <div class="row row-cols-3 justify-content-evenly">
            <?php if(!empty($data['products'])): ?>
                <?php foreach ($data['products'] as $product):?>
                    <div class="col border border-3 border-dark rounded bg-white p-1 m-1 justify-content-center w-25">
                        <div class="d-flex justify-content-center">
                            <img class="border-1 border border-dark rounded" src="<?php echo $product['image_products'];?>" alt="" width="200px" height="200px">
                        </div>

                        <div class="d-flex justify-content-start">
                            <h5><?php echo $product['name_products'];?></h5>
                        </div>

                        <div class="d-flex justify-content-between ">
                            <h5><?php echo $product['cost_products'];?> $</h5>
                            <input type="hidden" id="idProduct" value="<?php echo $product['id_products']; ?>">
                            <form id="paypalForm<?php echo $product['id_products']; ?>" target="_self" action="<?php echo $_ENV['PAYPAL_URL']; ?>" method="post">
                                <input type="hidden" name="business" value="<?php echo $_ENV['PAYPAL_ID']; ?>">

                                <input type="hidden" name="cmd" value="_cart">
                                <input type="hidden" name="add" value="1">

                                <input type="hidden" name="item_name" value="<?php echo $product['name_products']; ?>">
                                <input type="hidden" name="item_number" value="<?php echo $product['id_products']; ?>">
                                <input type="hidden" name="amount" value="<?php echo $product['cost_products']; ?>">
                                <input type="hidden" name="currency_code" value="USD">

                                <input type='hidden' name='cancel_return' value='<?php echo $_ENV['PAYPAL_CANCEL_URL']; ?>'>
                                <input type='hidden' name='return' value='<?php echo $_ENV['PAYPAL_RETURN_URL']; ?>'>

                                <button type="submit" id="addToCart" class="btn btn-outline-warning">Add to cart</button>
                            </form>
                        </div>
                        <?php if($_SESSION['user']->getRole() === 'admin'): ?>

                            <div class="d-flex justify-content-between mt-sm-3">
                                <a class="btn btn-outline-warning text-black" href="/deleteProduct?id=<?php echo $product['id_products'];?>">Delete product</a>
                                <a class="btn btn-outline-warning text-black" href="/updateProduct?id=<?php echo $product['id_products'];?>">Update product</a>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="alert alert-danger position-absolute top-50 start-50 translate-middle">
                    No products found
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php require_once 'footer.php'?>
<script src="app/js/addToCart.js"></script>
</body>
</html>

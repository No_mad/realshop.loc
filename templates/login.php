<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php require_once 'include_css.php' ?>
</head>
<body class="bg-secondary">
    <?php require_once 'header.php' ?>
        <div class="container position-absolute top-50 start-50 translate-middle w-50">
            <form action="/login" method="post" class="form-control border border-3 border-dark">
                <div class="d-flex justify-content-center">
                    <h1>Login</h1>
                </div>
                <label for="emailLogin">Email:</label>
                <input type="email" id="emailLogin" name="email" class="form-control" placeholder="Enter email" required>
                <label for="passwordLogin">Password</label>
                <input type="password" id="passwordLogin" name="password" class="form-control" placeholder="Enter password" required>
                Don`t have an account? <a href="/register">Register new one now!</a>
                <?php if(!empty($errors['error'])): ?>
                    <div class="alert alert-danger">
                        <?php echo $errors['error']; ?>
                    </div>
                <?php endif; ?>
                <br>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-outline-warning text-black w-25 mt-sm-3">Login</button>
                </div>
            </form>
        </div>
    <?php require_once 'footer.php' ?>
</body>
</html>
<?php $Sum = 0; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>My orders</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php require_once 'include_css.php' ?>
</head>
<body class="bg-secondary">
<?php require_once 'header.php' ?>
<div class="container">
    <div class="form-control border border-3 border-dark text-black">
        <?php if(!empty($data)): ?>
            <?php foreach ($data as $order): ?>
                <div class="row row-cols-4 mb-1">
                    <div class="col text-info">
                        #<?php echo $order['id_orders']; ?>
                    </div>
                    <div class="col">
                        <?php
                        if (!empty($return = \RealShop\Models\Product::getProductById($order['id_products']))) {
                            echo $return['name_products'];
                        }else {
                            echo 'error fetching product name';
                        }
                        ?>
                    </div>
                    <div class="col">
                        <?php
                        if (!empty($return['cost_products'])) {
                            $Sum += $return['cost_products'];
                            echo $return['cost_products'].'$';
                        }else{
                            echo 'error fetching product cost';
                        }
                        ?>
                    </div>
                    <div class="col">
                        <input type="hidden" id="idProduct" value="<?php echo $order['id_products']; ?>">
                        <form id="paypalForm<?php echo $order['id_products']; ?>" action="<?php echo $_ENV['PAYPAL_URL']; ?>" method="post">
                            <input type="hidden" name="business" value="<?php echo $_ENV['PAYPAL_ID']; ?>">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="item_name" value="<?php echo $return['name_products']; ?>">
                            <input type="hidden" name="item_number" value="<?php echo $order['id_products']; ?>">
                            <input type="hidden" name="amount" value="<?php echo $return['cost_products']; ?>">
                            <input type="hidden" name="currency_code" value="<?php echo $_ENV['PAYPAL_CURRENCY']; ?>">
                            <input type="hidden" name="return" value="<?php echo $_ENV['PAYPAL_RETURN_URL']; ?>">
                            <input type="hidden" name="cancel_return" value="<?php echo $_ENV['PAYPAL_CANCEL_URL']; ?>">
                            <input type="hidden" name="notify_url" value="<?php echo $_ENV['PAYPAL_NOTIFY_URL']; ?>">
                            <button type="submit" class="btn btn-outline-warning text-black">Reorder</button>
                        </form>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="row row-cols-4">
                <div class="col">

                </div>
                <div class="col">

                </div>
                <div class="col">
                    Total: <?php echo $Sum; ?>
                </div>
                <div class="col">
                    <a href="/exportMyOrders" class="btn btn-outline-warning text-black">Export .csv</a>
                </div>
            </div>
        <?php else: ?>
            <div class="alert alert-danger">
                No orders found
            </div>
        <?php endif; ?>
    </div>
</div>
<?php require_once 'footer.php' ?>

</body>
</html>
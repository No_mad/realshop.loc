<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php require_once 'include_css.php' ?>
</head>
<body class="bg-secondary">
<?php require_once 'header.php' ?>
<div class="container-fluid">
    <div class="container position-absolute top-50 start-50 translate-middle w-50">
        <form action="/register" method="post" class="form-control border border-3 border-dark">
            <div class="d-flex justify-content-center">
                <h1>Register</h1>
            </div>
            <label for="usernameLogin">Username:</label>
            <input type="text" id="usernameLogin" name="username" class="form-control" placeholder="Enter username" required>
            <label for="emailLogin">Email:</label>
            <input type="email" id="emailLogin" name="email" class="form-control" placeholder="Enter email" required>
            <label for="passwordLogin">Password</label>
            <input type="password" id="passwordLogin" name="password" class="form-control" placeholder="Enter password" required>
            Already have an account? <a href="/login">Log in!</a>
            <?php if(!empty($errors['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $errors['error']; ?>
                </div>
            <?php endif; ?>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-outline-warning text-black w-25 mt-sm-3">Register</button>
            </div>
        </form>
    </div>
</div>
<?php require_once 'footer.php' ?>
</body>
</html>

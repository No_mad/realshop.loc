<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php require_once 'include_css.php' ?>
</head>
<body class="bg-secondary">
<?php require_once 'header.php' ?>
<div class="container-fluid">
    <div class="container position-absolute top-50 start-50 translate-middle w-50">
        <form action="/updateProduct" method="post" class="form-control border border-3 border-dark">
            <div class="d-flex justify-content-center">
                <h1>Update product</h1>
            </div>
            <label for="nameProduct">Name:</label>
            <input type="text" id="nameProduct" name="name" class="form-control" placeholder="Enter name" required>
            <label for="costProduct">Cost:</label>
            <input type="number" id="costProduct" name="cost" class="form-control" placeholder="Enter cost" step=".01" required>
            <label for="imageProduct">Image url:</label>
            <input type="url" id="imageProduct" name="image" class="form-control" placeholder="Enter image url" required>
            <label for="categoryProduct">Category name:</label>
            <input type="text" id="categoryProduct" name="category" class="form-control" placeholder="Enter category name" required>
            <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
            <?php if(!empty($errors['error'])): ?>
                <div class="alert alert-danger">
                    <?php echo $errors['error']; ?>
                </div>
            <?php endif; ?>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-outline-warning text-black w-25 mt-sm-3">Update product</button>
            </div>
        </form>
    </div>
</div>
<?php require_once 'footer.php' ?>
</body>
</html>


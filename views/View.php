<?php

namespace RealShop\Views;

class View
{
    private string $fileExtension = 'php';
    private string $location = 'templates';

    public function render(string $viewName, array $data, array $errors)
    {
        require ROOT_PATH . DIRECTORY_SEPARATOR . $this->location . DIRECTORY_SEPARATOR . $viewName . '.' . $this->fileExtension;
    }

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    /**
     * @param string $fileExtension
     */
    public function setFileExtension(string $fileExtension): void
    {
        $this->fileExtension = $fileExtension;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }


}